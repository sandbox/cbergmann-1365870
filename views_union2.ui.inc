<?php
/**
 * @file
 * UI elements for views_union2
 *
 * This File contains all the functions needed for views_union2 to display its functions to the user.
 */
 
/**
 * Implements hook_form_FORM_ID_alter() for views_ui_edit_display_form().
 *
 * With this function I add the forms for adding and removing unions.
 * This is a rather unclean implementation but i saw no other method of injecting this setting into all display types.
 */
function views_union2_form_views_ui_edit_display_form_alter(&$form, &$form_state, $form_id  =  NULL) {
  $handler = $form_state["view"]->display[$form_state["display_id"]]->handler;
  
  switch ($form_state["section"]) {
    case 'union':
    $form["#title"] .= t("Union options.");

    $childs = _views_union2_get_child_views($form_state["view"], $form_state["view"]->current_display);
    if ($childs) {
      $options = array();
      foreach ($childs as $index => $child_view) {
        $view = $child_view["view"];
        $options[$index] = $view->name ."->". $view->display[$child_view["display"]]->display_title ."(". check_plain($child_view["args"]) .") ". (($child_view["use_all"]) ? "ALL" : "");
      }
      $form['child_views']  =  array(
        '#type' => 'checkboxes', 
        '#title' => t('Select checkbox to remove Union'), 
        '#options' => $options
      );
      
    }
    else{
      $form['empty'] = array(
        '#value' => t("No union added yet!")."<br />", 
      );
    }
    
    $form['addlink'] = array(
      '#value' => $handler->option_link(t("add Union"), "union_add"), 
    );
    
    $form["#submit"][] = "views_union2_union_options_submit";
    $form["buttons"]["submit"]["#submit"][] = "views_union2_union_submit";
    $form["buttons"]["submit"]["#value"] = t("Remove");
  break;
  
  case 'union_add':
    $views = array();
    $displays = array();
    foreach (views_get_all_views() as $view) {
      $views[$view->name] = $view->name;
      $options = array();
      foreach ($view->display as $display_id => $display) {
        $options[$display_id] = $display->display_title;
      }
      uasort($views, 'strcasecmp');
      $displays[$view->name .".display"] = array(
          '#prefix' => "<div class = \"views-dependent-all views-dependent-". $view->name ."\">", 
          '#suffix' => "</div>", 
          '#type' => 'select', 
          '#title' => "Display", 
          '#description' => "Display to use", 
          '#options' => $options
        );
    }
    uasort($views, 'strcasecmp');
    
    $form['view']  =  array(
      '#type' => 'select', 
      '#title' => t('View'), 
      '#options' => $views, 
      '#description' => t('View to use'), 
      '#attributes' => array('class' => "views-master-dependent"), 
      '#default_value' => $form_state["view"]->name, 
    );
        
    $form['display']  =  array(
      '#prefix' => '<div class = "views-radio-box form-radios" style = "height:80px">', 
      '#suffix' => '</div>', 
      '#tree' => TRUE, 
      '#title' => "Display", 
      '#default_value' => "default", 
    );
    foreach ($displays as $key => $value) {
      $form['display'][$key] = $value;
    }
    
    $form['args']  =  array(
      '#type' => 'textfield', 
      '#title' => t('Arguments'), 
      '#description' => t('Arguments that are send to the Display. Use "&lt;inherit&gt;" to propagate this views arguments down.'), 
    );
    
    $form['use_all']  =  array(
      '#type' => 'checkbox', 
      '#title' => 'ALL?', 
      '#description' => t('Should we use UNION ALL?'), 
    );
    
    $form["buttons"]["submit"]["#submit"][] = "views_union2_union_add_submit";
    $form["buttons"]["submit"]["#validate"][] = "views_union2_union_add_validate";
    $form["buttons"]["submit"]["#value"] = t("Add");
    
    $form["#submit"][] = "views_union2_union_add_submit";
    $form["#title"] .= t("Add a new Union.");
    
    
  break;
  }
  
  //move buttons to the bottom
  $buttons = $form["buttons"];
  unset($form["buttons"]);
  $form["buttons"] = $buttons;

}

/**
 * Implements hook_preprocess_HOOK() for views_ui_edit_tab().
 *
 * With this function I add the "Union" option into the General settings section of all displays.
 * This is a rather unclean implementation but i saw no other method of injecting this setting into all display types.
 */
function views_union2_preprocess_views_ui_edit_tab(&$vars) {
  $view  =  $vars['view'];
  $display  =  $vars['display'];
  $plugin  =  $display->handler->definition;
  
  $union = _views_union2_get_child_views($view, $vars['display']->id, FALSE);
  $link = $display->handler->option_link(($union) ? "Yes" : "No", "union");
  $vars["categories"]["basic"]["data"]["union"] = array();
  $data = &$vars["categories"]["basic"]["data"]["union"];
  $data["content"] = "Union: ". $link;
  $data['defaulted']  =  FALSE;
    $data['overridden']  =  FALSE;
    $data['class']  =  views_ui_item_css($vars['display']->id . '-union');
    if (!empty($view->changed_sections[$vars['display']->id . '-union'])) {
      $data['changed']  =  TRUE;
    }
  
  //fields are copied from childs so this field is disabled when Views Union is active
  if ($union) {
    foreach (_views_union2_inherit_options() as $type) {
      $title = t(ucfirst($type));
      $text = t("%title are disabled when you use Views Union as they are inherited from child views.", array("%title" => $title));
      $vars[$type] = '<div class = "views-category-title defaulted">'. $title .'</div><div class = "views-category-content defaulted">'. $text .'</div>';
    }
  }
}

/**
 * Additional form submission handler for views_ui_edit_display_form().
 *
 * This is called when removing unions.
 */
function views_union2_union_submit($form, &$form_state) {
  foreach ($form_state["values"]["child_views"] as $vuid => $value) {
  if ($value) {
    _views_union2_remove_child_view($form_state['view'], $form_state["display_id"], $vuid);
    //update UI
    $form_state['view']->changed_sections[$form_state["display_id"] ."-union"]  =  TRUE;
  }
  }
}

/**
 * Additional form submission handler for views_ui_edit_display_form().
 *
 * This is called when adding a new union.
 *
 * @see views_union2_union_add_validate()
 */
function views_union2_union_add_submit($form, &$form_state) {
  $options = array(
    'view' => $form_state["values"]["view"], 
    'display' => $form_state["values"]["display"][$form_state["values"]["view"] .".display"], 
    'use_all' => $form_state["values"]["use_all"], 
    'args' => $form_state["values"]["args"]
  );
  _views_union2_add_child_view($form_state["view"], $form_state["display_id"], $options);
  $form_state['view']->changed_sections[$form_state["display_id"] ."-union"]  =  TRUE;
}

/**
 * Additional form validation handler for views_ui_edit_display_form().
 *
 * @see views_union2_union_add_submit()
 */
function views_union2_union_add_validate($form, &$form_state) {
   //Validation stuff here, set $error to true if something went wrong, or however u want to do this.  Completely up to u in how u set errors.
   $view = views_get_view($form_state["values"]["view"]);
   $display = $form_state["values"]["display"][$form_state["values"]["view"] .".display"];
   if (empty($view->display[$display])) {
     form_set_error('display', 'This View does not have a Display with this name.');
   }
}
