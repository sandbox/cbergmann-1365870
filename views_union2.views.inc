<?php 
/**
 * @file
 * Declaration for views
 *
 * This File contains the meta information that the views module uses to identify the views_union2 module
 */
 
/**
 * Implements hook_views_handlers()
 */
function views_union2_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_union2'),
    ),
  );  
}
