Views Union 2
=============

This Module provides the ability UNIONize two or more views together.

Usage
-----
When you installed the module there is a new setting in each view that is called "Union". There you can add and remove sub views.
If you use Union with a view the Fields of this view will be delted because they are inherited from the child views.
